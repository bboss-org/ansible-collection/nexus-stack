# BBOSS Copyright (C) 2018-2021, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# Define default values
$hyper_venv = {
  "VENV_SCRIPT_PROJ" => nil,
  "VENV_HYPER_BOX_NUM" => 1,
  "VENV_HYPER_BOX_IMG" => "ubuntu/bionic64",
  "VENV_HYPER_BOX_VER" => ">= 0",
  "VENV_HYPER_BOX_CPU" => 4,
  "VENV_HYPER_BOX_MEM" => 4096,
  "VENV_HYPER_BOX_SSH" => "127.0.0.1",
  "VENV_HYPER_NET_FQDN" => nil,
  "VENV_HYPER_NET_PORT" => "8443",  # HTTPS only
  "VENV_PROXY_DOCKER" => nil,
}

# Read default values from last run
hyper_config = File.join(File.dirname(__FILE__), ".vagrant/hyper_config.rb")
if File.exist?(hyper_config)
  require hyper_config
end

# Non empty environment variable overrides default saved values
File.open(hyper_config, "w") do |line|
  $hyper_venv.each_key do |key|
    value = "#{ENV[key]}"
    if value.empty?
      value = $hyper_venv[key]
    else
      $hyper_venv[key] = value
    end
    # Save default values for the next run
    line.puts "$hyper_venv[\"#{key}\"] = \"#{value}\""
  end
end

# Define supported Vagrant box characteristics
hyper_boxes = {
  "centos/7" =>         {netif: "eth1"},    # CentOS 7
  "debian/stretch64" => {netif: "eth1"},    # Debian 9
  "debian/buster64"  => {netif: "eth1"},    # Debian 10
  "ubuntu/bionic64"  => {netif: "enp0s8"},  # Ubuntu 18.04 LTS
  "ubuntu/focal64"   => {netif: "enp0s8"},  # Ubuntu 20.04.1 LTS
}
hyper_boxes.default = hyper_boxes["ubuntu/bionic64"]
hyper_box = hyper_boxes[$hyper_venv["VENV_HYPER_BOX_IMG"]]

# Link content of ./inventory into .vagrant/provisioners/ansible/inventory to
# reuse group definitions and group_vars.
hyper_inventory = File.join(File.dirname(__FILE__),
  ".vagrant", "provisioners", "ansible", "inventory")
FileUtils.mkdir_p(hyper_inventory) if ! File.exist?(hyper_inventory)
Dir.foreach("inventory") do |item|
  ln_lnk = File.join(hyper_inventory, item)
  ln_trg = File.join("..", "..", "..", "..", "inventory", item)
  FileUtils.ln_s(ln_trg, ln_lnk) if ! File.exist?(ln_lnk)
end

Vagrant.configure("2") do |config|
  config.ssh.insert_key = false
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.cpus = 2
    vb.memory = 2048
  end

  # This host is the first deployment target, it has access to internet
  # througth an http/https proxy.
  config.vm.define "nexus-stack", primary: true  do |node|
    node.vm.provider "virtualbox" do |vb|
      vb.cpus = $hyper_venv["VENV_HYPER_BOX_CPU"]
      vb.memory = $hyper_venv["VENV_HYPER_BOX_MEM"]
    end
    node.vm.box = $hyper_venv["VENV_HYPER_BOX_IMG"]
    node.vm.box_version = $hyper_venv["VENV_HYPER_BOX_VER"]
    node.vm.hostname = "nexus-stack"
    node.vm.network "private_network", ip: "172.16.50.10"
    node.vm.network "forwarded_port", guest: 22, host: 2200, id: "ssh",
      host_ip: $hyper_venv["VENV_HYPER_BOX_SSH"], auto_correct: true
    node.vm.network "forwarded_port", 
      guest:  $hyper_venv['VENV_HYPER_NET_PORT'], 
      host:  $hyper_venv['VENV_HYPER_NET_PORT']
    node.vm.provision "hosts", :sync_hosts => true
    node.vm.provision "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      # Disable default limit to connect to all the machines
      ansible.limit = "all,localhost"
      ansible.groups = { 
        "nexus_proxy" => "nexus-stack",
        "nexus_client" => "nexus-client"
      }
      ansible.playbook = "playbooks/install.yml"
    end
  end

  # This host is the second deployment target.
  # No external http/https access.
  config.vm.define "nexus-client", autostart: false do |node|
    node.vm.box = $hyper_venv["VENV_HYPER_BOX_IMG"]
    node.vm.box_version = $hyper_venv["VENV_HYPER_BOX_VER"]
    node.vm.hostname = "nexus-client"
    node.vm.network "private_network", ip: "172.16.50.11"
    node.vm.network "forwarded_port", guest: 22, host: 2201, id: "ssh",
      host_ip: $hyper_venv["VENV_HYPER_BOX_SSH"], auto_correct: true
    # vagrant-hosts plugin
    # https://github.com/oscar-stack/vagrant-hosts
    node.vm.provision "hosts" do |hosts|
      hosts.autoconfigure = true
      hosts.sync_hosts = true
      hosts.add_host '172.16.50.10', [
        "nexus.#{$hyper_venv['VENV_HYPER_NET_FQDN']}",
        "dockerhub.#{$hyper_venv['VENV_HYPER_NET_FQDN']}"
      ]
    end
    node.vm.provision "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      # Disable default limit to connect to all the machines
      ansible.limit = "all,localhost"
      ansible.groups = { 
        "nexus_proxy" => "nexus-stack",
        "nexus_client" => "nexus-client"
      }
      ansible.playbook = "playbooks/test_client.yml"
    end
  end

  # This host is used to create the deployment package, it has access to
  # internet at least througth an http/https proxy.
  config.vm.define "package", autostart: false do |node|
    node.vm.box = $hyper_venv["VENV_HYPER_BOX_IMG"]
    node.vm.box_version = $hyper_venv["VENV_HYPER_BOX_VER"]
    node.vm.hostname = "package"
    node.vm.network "private_network", ip: "172.16.60.10"
    node.vm.network "forwarded_port", guest: 22, host: 2210, id: "ssh", 
      host_ip: $hyper_venv["VENV_HYPER_BOX_SSH"], auto_correct: true
    node.vm.provision "hosts", :sync_hosts => true
    node.vm.provision "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      # Disable default limit to connect to all the machines
      ansible.limit = "all,localhost"
      ansible.groups = {"build_package" => "package"}
      ansible.playbook = "playbooks/package.yml"
    end
  end

  # This host use the deployment package.
  # No external http/https access.
  # Deployments on targets are made from this host.
  config.vm.define "deploy", autostart: false do |node|
    node.vm.box = $hyper_venv["VENV_HYPER_BOX_IMG"]
    node.vm.box_version = $hyper_venv["VENV_HYPER_BOX_VER"]
    node.vm.hostname = "deploy"
    node.vm.network "private_network", ip: "172.16.60.11"
    node.vm.network "forwarded_port", guest: 22, host: 2211, id: "ssh",
      host_ip: $hyper_venv["VENV_HYPER_BOX_SSH"], auto_correct: true
    # Provisionnig vm
    node.vm.provision "hosts", :sync_hosts => true
    node.vm.provision "file" do |file|
      file.source = "#{ENV['HOME']}/.vagrant.d/insecure_private_key"
      file.destination = "~/.ssh/id_rsa"
    end
    node.vm.provision "ansible" do |ansible|
      ansible.compatibility_mode = "2.0"
      # Disable default limit to connect to all the machines
      ansible.limit = "all,localhost"
      ansible.groups = {"build_deploy" => "deploy"}
      ansible.playbook = "playbooks/deploy.yml"
    end
    node.vm.provision "shell" do |shell|
      shell.privileged = false
      shell.keep_color = true
      shell.env = {
        "ANSIBLE_FORCE_COLOR": "True",
      }
      shell.inline = <<-SHELL
        set -e
        cd ~/#{$hyper_venv['VENV_SCRIPT_PROJ']}/nexus-stack
        source ~/#{$hyper_venv['VENV_SCRIPT_PROJ']}/.venv/bin/activate
        export ANSIBLE_INVENTORY="../nexus-proxy/inventory/"
        export ANSIBLE_HOST_KEY_CHECKING="False"
        export VENV_SCRIPT_PROJ="#{$hyper_venv['VENV_SCRIPT_PROJ']}"
        export VENV_HYPER_NET_FQDN="#{$hyper_venv['VENV_HYPER_NET_FQDN']}"
        export VENV_HYPER_NET_PORT="#{$hyper_venv['VENV_HYPER_NET_PORT']}"
        export VENV_PROXY_DOCKER="#{$hyper_venv['VENV_PROXY_DOCKER']}"
        ansible-playbook playbooks/install.yml \
          -e proxy_http="#{ENV['http_proxy']}" \
          -e proxy_https="#{ENV['https_proxy']}"
        ansible-playbook playbooks/test_client.yml
      SHELL
    end
  end
end
